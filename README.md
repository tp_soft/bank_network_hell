# Transferwise Task 3 #

This documents steps necessary to get this application up and running.

### What is this repository for? ###

* Quick summary
 
This is for the task3 of the transferwise bootcamp tasks.

### How do I get set up? ###

#Summary of set up
IntelliJ IDE or other Java IDE / code editor
# Configuration
Used Java8 JRE/JDK on Maven
##Dependencies
###Some/all of these dependencies/library imports are used:
  + json_simple-1.1
   + com.google.code.gson
   + org.codehaus.jackson
   + org.json.simple.JSONObject
   + org.json.simple.parser.JSONParser
   + org.json.simple.parser.ParseException
   + org.json.simple.JSONArray
   + org.apache.http.client.methods.HttpPost
   + org.apache.http.client.methods.HttpGet
   + org.apache.http.impl.client.HttpClientBuilder
   + org.apache.http.HttpResponse
   + org.apache.http.client.HttpClient

##Database configuration
 + None
#How to run tests
 + None
#Deployment instructions
 - Java IDE configured with JRE and JDK.
  - The url to the REST service for the task should be active
  - The token for the task taker must be set

### Contribution guidelines ###
  - None

### Who do I talk to? ###