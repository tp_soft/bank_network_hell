import com.bank_netw_hell.work.Seeders.BankData;
import com.bank_netw_hell.work.Transaction.Transfer;


public class Main {



        public static void main(String[] args) {
            Transfer transfer = new Transfer();
            BankData bankData = new BankData();
            try {
                transfer.postMaker("http://bootcamp-api.transferwise.com/task/3/start?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a");
                bankData.doData("http://bootcamp-api.transferwise.com/bank?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a", "bankdata/bank.json");
                bankData.doData("http://bootcamp-api.transferwise.com/bankAccount?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a", "bankdata/bankaccount.json");
                bankData.doData("http://bootcamp-api.transferwise.com/payment?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a", "bankdata/payment.json");
                transfer.getPayments();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
}
