package com.bank_netw_hell.work.Seeders;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by omitobisam on 7.05.16.
 */
public class AccountSeed {
    String [][] accounts = new String[26][5];
    Long [] balance = new Long[26];

    public String[][] toAccounts(String fileName){
        String[][] accounts1 = new String[26][6];
        JSONParser parser = new JSONParser();
        int num = 0;

        try {
            JSONArray a = (JSONArray) parser.parse(new FileReader(fileName));

            for (Object o : a)
            {
                JSONObject acc = (JSONObject) o;
                    accounts1[num][0] = (String) acc.get("id");
                    accounts1[num][1] = (String) acc.get("accountNumber");
                    accounts1[num][2] = (String) acc.get("accountName");
                    accounts1[num][3] = (String) acc.get("bankId");
                    accounts1[num][4] = (String) acc.get("currency");
                    this.balance [num] = (Long) acc.get("balance");
                num++;
            }


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return accounts1;
    }

    public void setAccounts(){
        this.accounts = toAccounts("bankdata/bankaccount.json");
    }
    public String[][] getAccounts(){
        return this.accounts;
    }
    public Long[] getBalance() {
        return balance;
    }

    public void setBalance(Long[] balance) {
        this.balance = balance;
    }
}
