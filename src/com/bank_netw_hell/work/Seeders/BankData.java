package com.bank_netw_hell.work.Seeders;

/**
 * Created by omitobisam on 8.05.16.
 */

import java.io.*;
import java.net.URL;

public class BankData {
    public void doData(String url, String loc) throws Exception {
 /*       ObjectMapper mapper = new ObjectMapper();

        Map<String, Object> map = mapper.readValue("http://bootcamp-api.transferwise.com/bank?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a", Map.class);
        mapper.writeValue(new File("bankdata/bank.json"), map);
        Map<String, Object> map2 = mapper.readValue("http://bootcamp-api.transferwise.com/bankAccount?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a", Map.class);
        mapper.writeValue(new File("bankdata/bankaccount.json"), map2);
        Map<String, Object> map3 = mapper.readValue("http://bootcamp-api.transferwise.com/payment?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a", Map.class);
        mapper.writeValue(new File("bankdata/payment.json"), map3);*/

        InputStream input = null;
        OutputStream output = null;
        try {
            input = new URL(url).openStream();
            output = new FileOutputStream(loc);
            byte[] buffer = new byte[1024];
            for (int length = 0; (length = input.read(buffer)) > 0;) {
                output.write(buffer, 0, length);
            }
            // Here you could append further stuff to `output` if necessary.
        } finally {
            if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
            if (input != null) try { input.close(); } catch (IOException logOrIgnore) {}
        }
    }
}
