package com.bank_netw_hell.work.Seeders;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by omitobisam on 7.05.16.
 */
public class BankSeed {

    String [][] banks = new String[5][2];

    public String[][] toBanks(String fileName){
        String[][] banks1 = new String[5][2];
        JSONParser parser = new JSONParser();
        int num = 0;

        try {
            JSONArray a = (JSONArray) parser.parse(new FileReader(fileName));
            for (Object o : a)
            {
                JSONObject employee = (JSONObject) o;
                    banks1[num][0] = (String) employee.get("id");
                    banks1[num][1] = (String) employee.get("name");
                num++;
            }


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return banks1;
    }

    public void setBanks(){
        banks = toBanks("bankdata/bank.json");
    }
    public String[][] getBankDetails(){
        return this.banks;
    }

}
