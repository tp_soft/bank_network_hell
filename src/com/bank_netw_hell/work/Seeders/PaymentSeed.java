package com.bank_netw_hell.work.Seeders;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by omitobisam on 6.05.16.
 */
public class PaymentSeed {

    String [][] payments = new String[20][6];
    double [] amount = new double[20];

    public String[][] toPayments(String fileName){
        String[][] payments1 = new String[20][6];
        JSONParser parser = new JSONParser();
        int num = 0;

        try {
            JSONArray a = (JSONArray) parser.parse(new FileReader(fileName));
            for (Object o : a)
            {
                JSONObject payment = (JSONObject) o;
                payments1[num][0] = (String) payment.get("id");
                this.amount[num] = (Double.parseDouble(String.valueOf(payment.get("amount"))));
                payments1[num][1] = (String) payment.get("sourceCurrency");
                payments1[num][2] = (String) payment.get("targetCurrency");
                payments1[num][3] = (String) payment.get("recipientBankId");
                payments1[num][4] = (String) payment.get("iban");
                payments1[num][5] = (String) payment.get("created");
                num++;
            }


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return payments1;
    }

    public void setPayments(){
        this.payments = toPayments("bankdata/payment.json");
    }

    public String[][] getPayments(){
        return this.payments;
    }
    public double[] getAmount() {
        return amount;
    }

    public void setAmount(double[] amount) {
        this.amount = amount;
    }

}
