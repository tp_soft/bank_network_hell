package com.bank_netw_hell.work.Transaction;



import com.bank_netw_hell.work.Model.Transaction;
import com.bank_netw_hell.work.Seeders.AccountSeed;
import com.bank_netw_hell.work.Seeders.BankSeed;
import com.bank_netw_hell.work.Seeders.PaymentSeed;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStreamReader;


import static org.apache.http.protocol.HTTP.USER_AGENT;

/**
 * Created by omitobisam on 7.05.16.
 */
public class Transfer {
    String[][] from = new String[6][6];
    String[][] to = new String[21][6];

    public void getPayments(){

        PaymentSeed paymentSeed = new PaymentSeed();
        paymentSeed.setPayments();

        String [][]payments = paymentSeed.getPayments();
        double amount[] = paymentSeed.getAmount();


        BankSeed bankSeed = new BankSeed();
        bankSeed.setBanks();
        String [][] banks = bankSeed.getBankDetails();


        AccountSeed accountSeed = new AccountSeed();
        accountSeed.setAccounts();

        String [][] bankAccount = accountSeed.getAccounts();
        Long balance[] = accountSeed.getBalance();



        int n = 0;
        // Bank: "id   name"
        //Account: id	accountNumber	accountName	bankId	Currency
        //Payment: id	sourceCurrency	targetCurrency	recipientBankId	iban	created"

        Transaction trans = new Transaction();

        for (int num =0; num<payments.length; num++) {
            //
            trans.setAmount(amount[num]);
            //


            //
            for (int i = 0; i < bankAccount.length; i++) {
                if (bankAccount[i][4].equals(payments[num][1]) && bankAccount[i][2].contains("Transfer")) {
                    System.out.println(bankAccount[i][4]+ " "+bankAccount[i][1] +" " + bankAccount[i][2]);
                    String src = "";
                    src = bankAccount[i][1];
                    trans.setSourceAccountNo(bankAccount[i][1]);
                    if(!src.isEmpty()) break;
                        //System.out.println(payments[0][1] + " " + bankAccount[i][1]);
                }
            }
            //

            //
            for (int i = 0; i < bankAccount.length; i++) {
                if (bankAccount[i][4].equals(payments[num][1])) {
                    String bid = bankAccount[i][3];
                    for (int j = 0; j < banks.length; j++) {
                        if (banks[j][0].equals(bid)) {
                            trans.setBankname(banks[j][1]);
//                        System.out.println(banks[j][1]);
                        }
                    }
                }
            }
            //

            //
            trans.setTargetAccountNo(payments[num][4]);
            //

            //
            for (int i = 0; i < banks.length; i++) {
                if (banks[i][0].equals(payments[num][3])) {
//                System.out.println(payments[0][3]);
                    trans.setTargetBankName(banks[i][1]);
                }
            }
            //

            String bankname = trans.getBankname();
            String sourceAcountNo = trans.getSourceAccountNo();
            String targetAccountNumber = trans.getTargetAccountNo();
            String targetBankName = trans.getTargetBankName();
            double amnt = trans.getAmount();

            //System.out.println(bankname+" "+sourceAcountNo+" "+targetBankName+" "+targetAccountNumber+" "+amnt);
    /*    try {
            getMaker("http://bootcamp-api.transferwise.com/bank"+"?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a");
            getMaker("http://bootcamp-api.transferwise.com/bankAccount"+"?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a");
            getMaker("http://bootcamp-api.transferwise.com/payment"+"?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a");
        }catch (Exception e){
            e.printStackTrace();
        }*/
            makePayment(bankname, sourceAcountNo, targetBankName, targetAccountNumber, amnt);
        }
    }

    public void makePayment(String bankname, String sourceAccountNo, String targetBankName, String targetAccountNo, double amount) {

        String urlstring = "http://bootcamp-api.transferwise.com/bank/" + bankname +
                "/transfer/" + sourceAccountNo + "/" + targetBankName + "/" + targetAccountNo + "/" + amount+
                "?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a";
        String nUrl = urlstring.replace(" ", "%20");
        System.out.println(nUrl);
        try {
            postMaker(nUrl);
        } catch (Exception e){
            e.printStackTrace();
        }


    }

    public void postMaker(String url)throws Exception{
        //String url = "http://bootcamp-api.transferwise.com/task/3/start?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a";

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("User-Agent", USER_AGENT);
        HttpResponse response = client.execute(post);
        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        System.out.println(result);
    }

    public void getMaker(String url) throws Exception{
         //url = "http://bootcamp-api.transferwise.com/task/3?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a";

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);

        // add request header
        request.addHeader("User-Agent", USER_AGENT);
        HttpResponse response = client.execute(request);

        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result);
    }

}
