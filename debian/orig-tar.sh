#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
SOURCE_NAME=json-simple
VERSION=$2
DEBIAN_VERSION=$VERSION-dfsg1
UPSTREAM_SOURCE_DIR=$(echo $SOURCE_NAME | tr '-' '_')-${VERSION}-all
DEBIAN_SOURCE_DIR=${SOURCE_NAME}-$DEBIAN_VERSION
TAR=${USCAN_DESTDIR:-.}/${SOURCE_NAME}_$DEBIAN_VERSION.orig.tar.bz2

# clean up the upstream tarball
unzip $3 >/dev/null
# rename upstream source dir
mv ${UPSTREAM_SOURCE_DIR} ${DEBIAN_SOURCE_DIR}
# Remove excluded files
tar -c -j -X debian/orig-tar.exclude -f $TAR ${DEBIAN_SOURCE_DIR}/
rm -rf ${DEBIAN_SOURCE_DIR} $3
echo "${SOURCE_NAME}: Applied DFSG removals and set up tarball to `basename ${TAR}`"
