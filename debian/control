Source: json-simple
Section: java
Priority: extra
Maintainer: Gilles Filippini <pini@debian.org>
Build-Depends: debhelper (>= 7.0.50~), javahelper, default-jdk, ant,
 default-jdk-doc
Standards-Version: 3.9.1
Homepage: http://code.google.com/p/json-simple/
Vcs-Git: git://git.debian.org/pkg-java/json-simple.git
Vcs-Browser: http://git.debian.org/?p=pkg-java/json-simple.git;a=summary

Package: libjson-simple-java
Architecture: all
Depends: ${java:Depends}, ${misc:Depends}
Suggests: libjson-simple-doc
Description: Simple, lightweight and efficient JSON toolkit for Java
 While full featured and fully compliant with the JSON specification
 (RFC4627), JSON.simple aims to be as simple and efficient as possible.
 .
 It supports encoding, decoding and escaping JSON data and streams, and
 features a stoppable SAX-like interface to process JSON input.
 .
 It has been run through profiling tools to ensure high performance
 processing.
 .
 Targeted at JDK 6, JSON.simple is also compatible with JDK 1.2 and
 doesn't depend on any other Java library.

Package: libjson-simple-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends: default-jdk-doc
Description: documentation for libjson-simple-java
 This package contains the javadoc API documentation for libjson-simple-java,
 a simple, lightweight and efficient JSON toolkit for Java.
